package com.tinygray.tools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ToolsApplication {

    public static void main(String[] args) {
        //ConfigurableApplicationContext是ioc容器
        ConfigurableApplicationContext run = SpringApplication.run(ToolsApplication.class, args);
        //查看ioc容器里面的组件
        String[] beanDefinitionNames = run.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println(beanDefinitionName);
        }
        /* *
         *  @Configuration
         *  @Bean
         */
        /* //从容器里面获取组件 @Bean 是默认单例
        Student student = run.getBean("student", Student.class);
        Student student1 = run.getBean("student", Student.class);
        System.out.println(student == student1);
        //配置类（proxyBeanMethods = true 默认为true）情况下 调用方法  默认单例
        MyConfiguration myConfiguration = run.getBean("myConfiguration", MyConfiguration.class);
        //com.tinygray.tools.springbootLearn.configurationAnnotation.MyConfiguration$$EnhancerBySpringCGLIB$$1ed20a0d@4d95a72e myConfiguration表示是被SpringCGLIB增强了的代理对象
        System.out.println(myConfiguration);
        *//**
         *  如果@Configuration(proxyBeanMethods = true)   表示代理对象调用方法
         *      springboot总会检查这个组件是否在容器中存在 如果有就不会新创建对象
         *      目的：保证对象单实例
         *//*
        Student student2 = myConfiguration.getStudent();
        Student student3 = myConfiguration.getStudent();

        System.out.println(student2 == student3);*/
    }

}
