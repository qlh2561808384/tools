package com.tinygray.tools;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @Class:
 * @Description: URLTest$
 * @title: URLTest
 * @Author qlh
 * @Date: 2021/2/4 15:49
 * @Version 1.0
 */
public class URLTest {
    public static void main(String[] args) throws IOException {
        URL url = new URL("https://huya-w10.huya.com/2105/455443211/yuanhua/b15d0b3df3106172f5fcfa37269bcf36.mp4");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        InputStream inputStream = urlConnection.getInputStream();
        FileOutputStream fileOutputStream = new FileOutputStream("F:\\qlh.mp4");
        byte[] bytes = new byte[1024];
        int len ;
        while ((len=inputStream.read(bytes))!=-1){
            fileOutputStream.write(bytes, 0, len);
        }
        fileOutputStream.close();
        inputStream.close();
        urlConnection.disconnect();

    }
}
