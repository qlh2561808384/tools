package com.tinygray.tools.dicomToJpg;

import ij.plugin.DICOM;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Iterator;

public class DicomToJpg {
    public static void main(String args[]) {
        converse("E:\\test\\201907010932400013ABD.DCM");
    }
    private static void converse(String filePath) {
        try {
            DICOM dicom = new DICOM();
            dicom.run(filePath);
            BufferedImage bi = (BufferedImage) dicom.getImage();
            String imagePath = filePath + ".jpg";
            ImageWriter writer = null;
            Iterator iter = ImageIO.getImageWritersByFormatName("jpg");
            if (iter.hasNext()) {
                writer = (ImageWriter) iter.next();
            }
            writer.setOutput(new FileImageOutputStream(new File(imagePath)));
            ImageWriteParam iwparam = writer.getDefaultWriteParam();
            iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            iwparam.setCompressionQuality(1);
            writer.write(null, new IIOImage(bi, null, null), iwparam);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("错误" + e.getMessage());
        }

    }
}
