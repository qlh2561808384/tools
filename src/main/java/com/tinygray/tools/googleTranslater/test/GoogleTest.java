package com.tinygray.tools.googleTranslater.test;

import com.sun.org.apache.xpath.internal.operations.Gt;
import com.tinygray.tools.googleTranslater.GoogleApi;

public class GoogleTest {
    public static void main(String[] args) throws Exception {
        // 普通方式初始化
        GoogleApi googleApi = new GoogleApi();
        // 通过代理
        //GoogleApi googleApi = new GoogleApi("122.224.227.202", 3128);
        String result = googleApi.translate("nmn保健品123",  "en");
        System.out.println(result);
    }
}
