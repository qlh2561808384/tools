package com.tinygray.tools.spring5.aop;

import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */
public class JDKProxy {
    public static void main(String[] args) {
        //创建接口实现类的代理对象
        Class[] clazz = {UserDao.class};
        UserDaoImpl userDaoImpl = new UserDaoImpl();
        UserDao userDao = (UserDao) Proxy.newProxyInstance(JDKProxy.class.getClassLoader(), clazz, new UserDaoProxy(userDaoImpl));
        int add = userDao.add(1, 2);
        System.out.println(add);
    }
}
//创建代理对象
class UserDaoProxy implements InvocationHandler{
    //增强的逻辑
    /*
        1.创建的是谁的代理对象，把谁传过来  （传递方式：有参构造传递）
     */
    private Object object;
    public UserDaoProxy(Object object) {
        this.object = object;
    }
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //方法之前处理
        System.out.println("方法执行之前:方法名称" + method.getName() + "传递参数：" + Arrays.toString(args));
        //执行的方法
        Object invoke = method.invoke(object, args);

        //方法之后处理
        System.out.println("方法之后执行：" + object);
        return invoke;
    }
}
