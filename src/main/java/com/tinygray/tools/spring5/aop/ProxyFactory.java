package com.tinygray.tools.spring5.aop;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */

import java.lang.reflect.Proxy;

/**
 * JDK动态代理工厂
 */
public class ProxyFactory {
    //维护一个目标对象
    private Object object;

    //构造器 对object进行初始化
    public ProxyFactory(Object object) {
        this.object = object;
    }

    //给目标对象生成一个代理对象
    public Object getProxyInstance() {
        /**
         * 参数说明：
         *      ClassLoader loader:指定当前目标对象使用的类加载器，获取加载器的方法固定的
         *      Class<?>[] interfaces：目标对象实现的接口类型，使用泛型方法确认类型
         *      InvocationHandler h：是执行代理对象方法时，触发事件处理器
         */
        return Proxy.newProxyInstance(object.getClass().getClassLoader(), object.getClass().getInterfaces(), (proxy, method, args) -> {
            System.out.println("执行方法前--------JDK代理开始");
            //反射机制调用目标对象方法
            Object invoke = method.invoke(object, args );

            System.out.println("执行方法后--------JDK代理结束");
            return invoke;
        });
    }

}
