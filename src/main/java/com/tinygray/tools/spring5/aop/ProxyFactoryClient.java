package com.tinygray.tools.spring5.aop;

public class ProxyFactoryClient {
    public static void main(String[] args) {
        //创建目标对象
        UserDao userDao = new UserDaoImpl();
        ProxyFactory proxyFactory = new ProxyFactory(userDao);
        UserDao ud = (UserDao) proxyFactory.getProxyInstance();
        int add = ud.add(1, 2);
        System.out.println(add);
    }
}
