package com.tinygray.tools.spring5.aop.aspectjanno;

import org.springframework.stereotype.Component;

/**
 * 要被增强的类
 */
@Component
public class User {
    public void add (){
        System.out.println("被增强方法");
    }

}
