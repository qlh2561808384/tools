package com.tinygray.tools.spring5.aop.aspectjanno;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 * @Class:
 * @Description: UserProxy$
 * @title: UserProxy
 * @Author qlh
 * @Date: 2021/3/4 17:22
 * @Version 1.0
 */
@Component
@Aspect
public class UserProxy {
    @Pointcut(value = "execution(* com.tinygray.tools.spring5.aop.aspectjanno.User.add(..))")
    public void pointcut() {

    }

    @Before("pointcut()")
    public void doBefore(JoinPoint joinPoint) {
        System.out.println("执行了前置通知。。。。方法之前执行");
    }

    /**
     * 方法执行之后执行
     *      不管你方法是否由异常 都会执行
     */
    @After("pointcut()")
    public void doAfter(){
        System.out.println("@After：执行了后置通知。。。。方法之后执行");
    }
    /**
     * 方法执行之后 返回结果时执行
     *      方法异常 不执行
     *      方法不异常  执行
     */
    @AfterReturning("pointcut()")
    public void doAfterReturn(){
        System.out.println("@AfterReturning():执行了后置返回通知。。。。方法返回值之后执行后");
    }
    /**
     * 方法执行之后 异常执行
     *      方法异常 执行
     */
    @AfterThrowing("pointcut()")
    public void doAfterThrowing(){
        System.out.println("@AfterThrowing:执行了后置异常通知。。。。");
    }
    /**
     * 方法执行之后 异常执行
     *      方法异常 执行
     */
    @Around("pointcut()")
    public void doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("@Around:执行了环绕。。。。");
        System.out.println("方法之前执行环绕之前。。。");
        //被增强的方法
        proceedingJoinPoint.proceed();

        System.out.println("方法之后执行环绕之后。。。");

    }
}
