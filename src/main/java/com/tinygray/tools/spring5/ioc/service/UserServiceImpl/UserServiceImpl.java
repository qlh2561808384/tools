package com.tinygray.tools.spring5.ioc.service.UserServiceImpl;

import com.tinygray.tools.spring5.ioc.UserDao.UserDao;
import com.tinygray.tools.spring5.ioc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */
public class UserServiceImpl implements UserService {
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    private UserDao userDao;
    @Override
    public void add() {
        System.out.println("service 的 add方法");
        userDao.update();
    }
}
