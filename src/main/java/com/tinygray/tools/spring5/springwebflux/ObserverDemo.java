package com.tinygray.tools.spring5.springwebflux;


import java.util.Observable;
import java.util.Observer;

public class ObserverDemo extends Observable {
    public static void main(String[] args) {
        ObserverDemo observerDemo = new ObserverDemo();
        observerDemo.addObserver((o, arg) -> {
            System.out.println("发生变化");
        });
        observerDemo.addObserver((o, arg) -> {
            System.out.println("发生变化");
        });
    }
}
