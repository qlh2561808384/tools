package com.tinygray.tools.springbootLearn.application;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *  @PropertySource(value = "classpath:custom-profile.properties")
 *      重要重要重要：：：：里面加载的配置文件必须是"xxxx.properties"类型的。不能是yml、yaml等等其他文件（暂时了解）
 *                        如果不是"xxxx.properties"类型的  类属性上必须要加上@Value注解获取属性值  而且容易乱码
 *                         所以建议使用"xxxx.properties"类型的
 *
 *      比如：
 *           @ConfigurationProperties(prefix = "dog")
 *           加载application.properties下面的dog开头的属性
 *      指定的情况下：加载@PropertySource 指定的配置文件下面的属性
 *      比如：
 *          @PropertySource("classpath:custom-profile.yml")
 *          @ConfigurationProperties(prefix = "dog")
 *          加载custom-profile.properties下面的dog开头的属性
 *  一般情况下 为了application.properties配置文件简洁  该配置文件下面最好放springboot自动装配的属性
 *  其他的自定义的属性 重新写一个配置文件  这样更加清晰明了  不至于很乱
 *  当然了 也可以在application.yml配置文件里面写
 */
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
//@PropertySource("classpath:qlh.yml") 下面属性要用@Value赋值
@PropertySource("classpath:custom-profile.properties")
@ConfigurationProperties(prefix = "person")
public class Person {

    private String name;
    private int age;
    private Date birthday;
    private List<String> list;
    private Map<String, Object> map;
    private Dog dog;
}
