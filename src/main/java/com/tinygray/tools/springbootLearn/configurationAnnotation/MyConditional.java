package com.tinygray.tools.springbootLearn.configurationAnnotation;

import com.tinygray.tools.springbootLearn.configurationAnnotation.bean.Person;
import com.tinygray.tools.springbootLearn.configurationAnnotation.bean.Student;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 条件装配：满足Conditional指定的条件，则进行组件注入
 *
 * @ConditionalOnBean() 容器里面有里面的bean的情况下 才进行方法里面组件注册
 */
@Configuration
@ConditionalOnBean({Student.class})
public class MyConditional {
    @Bean
    public Person getPerson() {
        return new Person("机器人");
    }

    //    @Bean("student")
    public Student getStudent() {
        return new Student("齐龙辉");
    }
}
