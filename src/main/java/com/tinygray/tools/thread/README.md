# 线程小结 `线程实现三种方式`
1. **继承Thread类实现线程**
    + 子类继承Thread类具备多项能力
    + 启动线程：子类对象.start()
    + 不建议使用：避免OOP单继承局限性
2. **实现Runnable接口实现线程**（最重要的）
    + 实现Runnable接口具备多线程能力
    + 启动线程：传入目标对象+Thread对象.start()
    + 推荐使用：避免单继承局限性，灵活方便。方便同一个对象被多个线程使用
    ```
           //创建Runnable接口实现类的对象
           TestThread2 testThread1 = new TestThread2();
           TestThread2 testThread2 = new TestThread2();
           TestThread2 testThread3 = new TestThread2();
           //创建线程对象  通过线程对象开启我们的线程，原理：通过代理模式实现线程
           /*Thread thread = new Thread(testThread1);
           thread.start();*/
           new Thread(testThread1).start();
           new Thread(testThread2).start();
           new Thread(testThread3).start();
    ```
3. 实现Callable接口（学习阶段了解就可以 非常重要-工作3-5年后 变成核心实现）
