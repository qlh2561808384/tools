package com.tinygray.tools.thread;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */

import java.util.concurrent.Callable;

/**
 * 线程创建方式三：实现callable接口
 */
public class TestCallable implements Callable {

    @Override
    public Object call() throws Exception {
        return null;
    }

}
