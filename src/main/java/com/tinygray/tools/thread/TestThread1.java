package com.tinygray.tools.thread;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */

/**
 * 创建线程方式一
 * 继承Thread类
 * 重写run方法
 * 调用start方法开启线程
 */
public class TestThread1 extends Thread {
    @Override
    public void run() {
        //run方法线程体
        for (int i = 0; i < 200; i++) {
            System.out.println("我是run方法线程"+ TestThread1.currentThread());
        }

    }

    public static void main(String[] args) {
        //main方法主线程

        //
        /**
         * 创建一个线程对象调用start方法
         *      testThread.start():start()方法 多条执行路径 主线程跟子线程交替进行
         *      testThread.run():run()方法  只有主线成一条路径 （执行到run()方法时候 去调用run()方法里面的线程）
         */

        TestThread1 testThread = new TestThread1();
        testThread.start();
        for (int i = 0; i <200 ; i++) {
            System.out.println("我是main主线程" + TestThread1.currentThread());
        }
    }
}
