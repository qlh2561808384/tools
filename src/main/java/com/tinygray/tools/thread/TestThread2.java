package com.tinygray.tools.thread;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */
/**
 * 创建线程方式二
 * 实现Runnable接口
 * 重写run方法
 * 执行线程需要丢入Runnable接口实现类，调用start()方法
 */
public class TestThread2 implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 200; i++) {
            System.out.println("我是run方法线程"+ TestThread1.currentThread());
        }
    }

    public static void main(String[] args) {
        //创建Runnable接口实现类的对象
        TestThread2 testThread1 = new TestThread2();
        //创建线程对象  通过线程对象开启我们的线程，原理：通过代理模式实现线程
        /*Thread thread = new Thread(testThread1);
        thread.start();*/
        new Thread(testThread1).start();
        for (int i = 0; i < 200; i++) {
            System.out.println("我是main主线程" + TestThread1.currentThread());
        }
    }
}
