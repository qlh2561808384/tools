package com.tinygray.tools.thread;

import java.util.concurrent.Callable;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */

/**
 * 线程创建方式三：实现Callable接口
 */
public class TestThread3 implements Callable {
    @Override
    public Object call() throws Exception {
        return null;
    }
}
