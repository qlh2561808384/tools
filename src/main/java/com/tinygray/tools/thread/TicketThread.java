package com.tinygray.tools.thread;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */

/**
 * 多个线程同时操作一个对象
 * 购买火车票的例子
 *
 * 发现问题：
 *      多个线程操作同一个资源的情况下，线程不安全 数据紊乱（并发的问题）
 */
public class TicketThread implements Runnable {
    //火车票数
    private int ticketNums = 10;
    @Override
    public void run() {
        while (ticketNums > 0) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "拿到了第" + ticketNums-- + "票");
        }
    }

    public static void main(String[] args) {
        TicketThread ticketThread = new TicketThread();
        new Thread(ticketThread,"米米").start();
        new Thread(ticketThread,"花花").start();
        new Thread(ticketThread,"狗狗").start();

    }

}
