package com.tinygray.tools.thread.staticProxy;

/**
 *
 *
 * @return
 * @author qlh
 * @公众号 madison龙少
 * @CSDN https://blog.csdn.net/qlh2561808384
 * @B站 https://space.bilibili.com/54224726
 * @creed: Talk is cheap,show me the code
 * @date
 */

/**
 *  静态代理
 *          真实对象跟代理对象都要实现同一个接口
 *          代理对象要代理真是角色
 *
 */
public class StaticProxy {
    public static void main(String[] args) {
        new Thread(() -> System.out.println("我爱你")).start();
        new WeddingCompany(new You()).happyMerry();
    }
}
interface Merry{
    void happyMerry();
}

/**
 *  真是角色 ： 我结婚
 */
class You implements Merry {
    @Override
    public void happyMerry() {
        System.out.println("我要结婚了");
    }
}

/**
 *  代理角色：婚庆公司帮助你结婚
 */
class WeddingCompany implements Merry{
    public WeddingCompany(Merry target) {
        this.target = target;
    }
    //代理谁--->真实对象
    private Merry target;

    @Override
    public void happyMerry() {
        before();
        //这就是真实对象
        this.target.happyMerry();
        after();
    }

    /**
     *  结婚之后
     */
    private void after() {
        System.out.println("结婚之后：婚庆公司收尾款");
    }

    /**
     * 结婚之前
     */
    private void before() {
        System.out.println("结婚之前：布置现场");
    }
}
