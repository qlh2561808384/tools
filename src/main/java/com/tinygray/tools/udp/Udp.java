package com.tinygray.tools.udp;

import com.alibaba.fastjson.JSONObject;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Udp {
    public static String ObtainPythonResultByUdp(JSONObject jsonObject){
        String msg = jsonObject.toString();
        byte[] buf = msg.getBytes();
        String result = "";
        try {
            InetAddress address = InetAddress.getByName("39.105.98.101");  //服务器地址
            address = InetAddress.getLocalHost();
            int port = 62000;  //服务器的端口号
            //创建发送方的数据报信息
            DatagramPacket dataGramPacket = new DatagramPacket(buf, buf.length, address, port);

            DatagramSocket socket = new DatagramSocket();  //创建套接字
            socket.send(dataGramPacket);  //通过套接字发送数据

            //接收服务器反馈数据
            byte[] backbuf = new byte[1024];
            DatagramPacket backPacket = new DatagramPacket(backbuf, backbuf.length);
            socket.receive(backPacket);  //接收返回数据
            result = new String(backbuf, 0, backPacket.getLength());
            System.out.println("服务器返回的数据为:" + result);
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
