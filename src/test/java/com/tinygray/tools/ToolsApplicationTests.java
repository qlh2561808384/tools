package com.tinygray.tools;


import com.tinygray.tools.spring5.aop.aspectjanno.User;
import com.tinygray.tools.spring5.ioc.service.UserService;
import com.tinygray.tools.springbootLearn.application.Dog;
import com.tinygray.tools.springbootLearn.application.Person;
import net.bytebuddy.asm.Advice;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootTest
class ToolsApplicationTests {

    @Test
    void contextLoads() {
    }
    @Test
    void getUser(){
        ApplicationContext context = new ClassPathXmlApplicationContext("springConfig/bean1.xml");
        UserService userService = context.getBean("UserServiceImpl", UserService.class);
        System.out.println(userService);
        userService.add();
    }
    @Test
    void getUserAop(){
        ApplicationContext context = new ClassPathXmlApplicationContext("springConfig/bean1.xml");
        User user = context.getBean("user", User.class);
        user.add();
    }
    @Autowired
    private Dog dog;
    @Autowired
    private Person person;
    @Test
    public void applicationPropertiesTest(){
        System.out.println(dog);
        System.out.println(person);
    }

}
